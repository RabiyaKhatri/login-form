import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LoginForm from "../components/form";
import Welcome from "../components/welcome";

export const Routing = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <LoginForm />
        </Route>
        <Route exact path="/welcome">
          <Welcome />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};
