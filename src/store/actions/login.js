import axios from "axios";

export const LoginActionTypes = {
  USER_DATA: "User-data",
};

export const SetUserData = (payload) => {
  return {
    type: LoginActionTypes.USER_DATA,
    payload,
  };
};

export const User = (body) => (dispatch, getState) => {
  return axios
    .post("https://xfoil-technical-interview.herokuapp.com/login", body)
    .then((res) => {
      console.log("res", res, body);

      dispatch(SetUserData(res.data));
    })
    .catch((err) => {
      console.log("error", err);
    });
};
