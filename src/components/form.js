import React from "react";
import { Form, Input, Button, message } from "antd";
import { User } from "../store/actions";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";



const LoginForm = ({ addUser }) => {
  let histroy = useHistory();

  const onFinish = (values) => {
    values = { username: values.username, password: parseInt(values.password) };
    addUser(values);
    if (values.username === "demo@xfoil.com" && values.password === 123) {
      histroy.push("/welcome");
    } else {
      message.error("Incorrect Email or Password");
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      typeof="post"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addUser: (body) => dispatch(User(body)),
  };
};

export default connect(mapDispatchToProps)(LoginForm);
