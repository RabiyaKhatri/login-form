import { Typography, Space, List } from "antd";
import React from "react";
import { connect } from "react-redux";

const { Title, Text } = Typography;

const Welcome = ({ userData }) => {
  console.log("user", userData);
  return (
    <div>
      <Space direction="vertical">
        <Title level={2}>Welcome</Title>
        <Title level={4}>
          {userData.firstNameAdmin} {userData.lastNameAdmin}
        </Title>

        <Text>Your company Id is {userData.companyId}</Text>
        <List
          size="large"
          header={<Text>Location</Text>}
          bordered
          dataSource={userData.locationObjects}
          renderItem={(item) => <List.Item>{item.locationName}</List.Item>}
        />
      </Space>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    userData: state.login.userData,
  };
};

export default connect(mapStateToProps)(Welcome);
