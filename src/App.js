import "antd/dist/antd.css";
import { Provider } from "react-redux";
import "./App.css";
import { Routing } from "./router";
import store from "./store";


function App() {
  return (
    <div>
      <Provider store={store}>
       
        <Routing />
      </Provider>
    </div>
  );
}

export default App;
